--Cria o banco de dados desafio
create database desafio;
-- cria o usuario para conectar ao SGBD
create role api with superuser encrypted password 'api';
alter role api with login;
--troca o proprietario do banco para o usuario api
alter database desafio owner to api;
