<?php

use Illuminate\Database\Seeder;

class UsuarioSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\usuario::create([
            'login' => str_random(10),
            'nome' => str_random(10),
            'cpf' => '01234567809',
            'email' => str_random(11).'@xyz.zz',
            'endereco' => str_random(10),
            'senha' => bcrypt('secret')]);
    }
}
