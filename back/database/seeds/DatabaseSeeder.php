<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        Model::unguard();

        $this->call(EmpresaSeed::class);
        $this->call(UsuarioSeed::class);
        $this->call(UsuarioEmpresaSeed::class);

        Model::reguard();
    }
}
