<?php

use Illuminate\Database\Seeder;

class UsuarioEmpresaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\usuarioEmpresa::create([
            'idempresa' => 1,
            'idusuario' => 1]);
    }
}
