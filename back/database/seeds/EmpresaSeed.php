<?php

use Illuminate\Database\Seeder;

class EmpresaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          App\empresa::create([
            'nome' => str_random(10),
            'cnpj' => '01234567809123',
            'endereco' => str_random(14)]);
    }
}
