<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaUsuarioEmpresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idempresa');
            $table->integer('idusuario');
//             $table->primary(['idempresa', 'idusuario']);
            $table->foreign('idempresa')
                ->references('id')
                ->on('empresas')
                ->onDelete('cascade');
            $table->foreign('idusuario')
                ->references('id')
                ->on('usuarios')
                ->onDelete('cascade');
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usuario_empresas');
    }
}
