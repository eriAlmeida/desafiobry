<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class empresa extends Model
{
    protected $fillable = ['nome', 'cnpj', 'endereco'];

    protected $dates = ['deleted_at'];

    public function usuario()
    {
//         return $this->hasMany('App\usuario');
           return $this->belongsToMany('App\usuario','usuario_empresas','idempresa','idusuario');
    }
}
