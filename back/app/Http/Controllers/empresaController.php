<?php

namespace App\Http\Controllers;

use App\empresa;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

class empresaController extends Controller
{
    public function index()
    {
        $Empresa = empresa::with('usuario')->get();
        return response()->json($Empresa);
    }
    
    public function show($id)
    {
        $Empresa = empresa::with('usuario')->find($id);

        if(!$Empresa) {
            return response()->json([
                'mensagem'   => 'Registro não encontrado',
            ], 404);
        }

        return response()->json($Empresa);
    }
    
    public function store(Request $request)
    {
        $Empresa = new empresa();
        $Empresa->fill($request->all());
        $Empresa->save();

        return response()->json($Empresa, 201);
    }
    
     public function update(Request $request, $id)
    {
        $Empresa = empresa::find($id);

        if(!$Empresa) {
            return response()->json([
                'mensagem'   => 'Registro não encontrado',
            ], 404);
        }

        $Empresa->fill($request->all());
        $Empresa->save();

        return response()->json($Empresa);
    }
    
      public function destroy($id)
    {
        $Empresa = empresa::find($id);
       
        if(!$Empresa) {
            return response()->json([
                'mensagem'   => 'Registro não encontrado',
            ], 404);
        }

        $Empresa->delete();
        return response()->json([
                'mensagem'   => 'Registro '.$id.' removido ',
            ], 200);
    }
}
