<?php

namespace App\Http\Controllers;

use App\usuario;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

class usuarioController extends Controller
{
     public function index()
    {
        $Usuario = usuario::with('empresa')->get();
        return response()->json($Usuario);
    }
    
    public function show($id)
    {
        $Usuario = usuario::with('empresa')->find($id);

        if(!$Usuario) {
            return response()->json([
                'mensagem'   => 'Registro não encontrado',
            ], 404);
        }

        return response()->json($Usuario);
    }
    
     public function store(Request $request)
    {
        $Usuario = new usuario();
        $Usuario->fill($request->all());
        $Usuario->save();

        return response()->json($Usuario, 201);
    }
    
    public function update(Request $request, $id)
    {
        $Usuario = usuario::find($id);

        if(!$Usuario) {
            return response()->json([
                'mensagem'   => 'Registro não encontrado',
            ], 404);
        }

        $Usuario->fill($request->all());
        $Usuario->save();

        return response()->json($Usuario);
    }
    
     public function destroy($id)
    {
         $Usuario = usuario::find($id);
        
        if(!$Usuario) {
            return response()->json([
                'mensagem'   => 'Registro não encontrado',
            ], 404);
        }

        $Usuario->delete();
        return response()->json([
                'mensagem'   => 'Registro '.$id.' removido ',
            ], 200);
    }
}
