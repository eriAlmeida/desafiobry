<?php

namespace App\Http\Controllers;

use App\usuarioEmpresa;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

class associacaoController extends Controller
{
      public function index()
    {
        $UsuarioEmpresa = usuarioEmpresa::all();
        return response()->json($UsuarioEmpresa);
    }
    
    public function show($id)
    {
        $UsuarioEmpresa = usuarioEmpresa::find($id);

        if(!$UsuarioEmpresa) {
            return response()->json([
                'mensagem'   => 'Registro não encontrado',
            ], 404);
        }

        return response()->json($UsuarioEmpresa);
    }
     
     public function store(Request $request)
    {
        $UsuarioEmpresa = new usuarioEmpresa();
        $consulta = DB::table('usuario_empresas')
                    ->where([['idempresa','=', $request->idempresa],['idusuario','=',$request->idusuario]])                      ->get();           
         if($consulta) {
            return response()->json([
                'mensagem'   => 'Associacao já realizada',
            ], 404);
         }else{        
            $UsuarioEmpresa->fill($request->all());
            $UsuarioEmpresa->save();
            return response()->json($UsuarioEmpresa, 201);
         }

    }
    
     public function destroy($id)
    {
        $UsuarioEmpresa = usuarioEmpresa::find($id);
         
        if(!$UsuarioEmpresa) {
            return response()->json([
                'mensagem'   => 'Registro não encontrado',
            ], 404);
        }

        $UsuarioEmpresa->delete();
        return response()->json([
                'mensagem'   => 'Registro '.$id.' removido ',
            ], 200);
    }
}
