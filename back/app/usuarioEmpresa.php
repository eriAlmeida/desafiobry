<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usuarioEmpresa extends Model
{
    protected $table      = 'usuario_empresas';

//     protected $primaryKey = ['idempresa','idusuario'];

    protected $fillable = ['idempresa','idusuario'];
    
//     public $incrementing = false;
    
    public  $timestamps   = false;
    
}
