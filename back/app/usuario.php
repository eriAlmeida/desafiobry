<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario extends Model
{
    protected $fillable = ['login', 'nome', 'cpf', 'email', 'endereco','senha'];

    protected $hidden = ['senha'];

    protected $dates = ['deleted_at'];

    public function empresa()
    {
//         return $this->hasMany('App\empresa');
        return $this->belongsToMany('App\empresa','usuario_empresas','idusuario','idempresa');
    }
}
