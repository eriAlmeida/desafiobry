import { Injectable } from '@angular/core';

import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Empresa } from 'src/model/empresa';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

// const apiUrl = 'http://localhost/back/server.php/api';
const apiUrl = 'http://127.0.0.1:8887/Get_geral_empresas.json';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

    constructor(private http: HttpClient) { }

  getEmpresa (): Observable<Empresa[]> {
//     return this.http.get<Empresa[]>(apiUrl+'/empresa')
    return this.http.get<Empresa[]>(apiUrl)
      .pipe(
        tap(empresa => console.log('leu a empresa')),
        catchError(this.handleError('getEmpresas', []))
      );
  }

  getEmpresaID(id: number): Observable<Empresa> {
    const url = `${apiUrl+'/empresa'}/${id}`;
    return this.http.get<Empresa>(url).pipe(
      tap(_ => console.log(`leu a empresa id=${id}`)),
      catchError(this.handleError<Empresa>(`getEmpresa id=${id}`))
    );
  }

  addEmpresa (empresa): Observable<Empresa> {
    return this.http.post<Empresa>(apiUrl+'/empresa', empresa, httpOptions).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((empresa: Empresa) => console.log(`adicionou a empresa com w/ id=${empresa.id}`)),
      catchError(this.handleError<Empresa>('addEmpresa'))
    );
  }

  updateEmpresa(id, empresa): Observable<any> {
    const url = `${apiUrl+'/empresa'}/${id}`;
    return this.http.put(url, empresa, httpOptions).pipe(
      tap(_ => console.log(`atualiza a empresa com id=${id}`)),
      catchError(this.handleError<any>('updateEmpresa'))
    );
  }

  deleteEmpresa (id): Observable<Empresa> {
    const url = `${apiUrl+'/empresa'}/${id}`;

    return this.http.delete<Empresa>(url, httpOptions).pipe(
      tap(_ => console.log(`remove a empresa com id=${id}`)),
      catchError(this.handleError<Empresa>('deleteEmpresa'))
    );
  }

   private handleError<T> (operation = 'operation', result?: T){
      return (error: any): Observable<T> => {
        console.error(error);
        return of(result as T);
      };
  }
}
