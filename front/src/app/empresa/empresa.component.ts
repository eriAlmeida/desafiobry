import { Component, OnInit } from '@angular/core';
import { Empresa } from 'src/model/empresa';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.scss']
})
export class EmpresaComponent implements OnInit {


  empresa: Empresa;

  constructor(private _apiService: ApiService) {}
       ListarEmpresa(): void{
        console.log(this._apiService.getEmpresa().subscribe((data: this.empresa) => this.empresa = data,error => console.log(error)));

       }
  ngOnInit() {
  }

}
