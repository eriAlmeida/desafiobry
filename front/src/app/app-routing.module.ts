import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioComponent } from './usuario/usuario.component';
import { EmpresaComponent } from './empresa/empresa.component';
import { UsuarioEmpresaComponent } from './usuario-empresa/usuario-empresa.component';

const routes: Routes = [
    {
    path: 'empresa',
    component: EmpresaComponent,
    data: { title: 'Lista de Empresas' }
  },
  {
    path: 'usuario',
    component: UsuarioComponent,
    data: { title: 'Lista de usuarios' }
  },
  {
    path: 'associacao',
    component: UsuarioEmpresaComponent,
    data: { title: 'Associar entre empresas e usuarios' }
  },
  { path: '',
    redirectTo: '/',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
