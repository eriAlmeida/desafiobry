import { Component } from '@angular/core';

import { Observable, of, throwError } from 'rxjs';
import { Empresa } from 'src/model/empresa';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'DesafioBry';
  constructor() { }

}
