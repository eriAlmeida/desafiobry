export class Empresa {
    id: string;
    nome: string;
    cnpj: string;
    endereco: string;
}
