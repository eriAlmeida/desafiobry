# DesafioBRY
<p><b>Desenvolvido com<b></p>
<hr>
<p>Laravel : v5.2.31</p>
<p>PHP : PHP 7.2.15-0ubuntu0.18.04.2</p>
<p>Apache : Apache/2.4.29</p>
<p>SGBD :PostgreSQL 11.2</p> 
<p>Codificação : UTF8</p>
<hr>
<p><b>Instala&ccedil;&atilde;o<b></p>
<hr>
<p><i>Estrutura do Banco</i></p>
<hr>
<p>dentro do diretorio instalarDB existe o arquivo criaDB.sql este arquivo deve ser executado com o "\i" exemplo \i criaDB.sql com usuário postgres vai criar o banco, o usuário usado pelo REST e dar permissão de acesso ao usuário  </p>
<p>Criando as tabelas com  Laravel <code>php artisan migrate<code></p>
<p>Populando com exemplos <code>php artisan db:seed<code></p>
<hr>
<p><b>Execucao REST laravel</b></p>
<hr>
<p><i>ROTAS</i></p>
<code>
    api/empresa     verbo GET    -- Lista todas as empresas com usuários atrelado(s) a ela
    api/empresa/id  verbo GET    -- Lista a empresas ou retorna "id não cadastrado"
    api/empresa     verbo POST   -- Insere uma nova empresa
    api/empresa/id  verbo PUT    -- Altera a informacao da empresa ou retorna "id não cadastrado"
    api/empresa/id  verbo DELETE -- Exclui uma empresa ou retorna "id não cadastrado"
    
    api/usuario     verbo GET    -- Lista todos os usuários com as empresa(s) atrelados a ele
    api/usuario/id  verbo GET    -- Lista os usuarios ou retorna "id não cadastrado"
    api/usuario     verbo POST   -- Insere um novo usuário 
    api/usuario/id  verbo PUT    -- Altera a informacao do usuário ou retorna "id não cadastrado"
    api/usuario/id  verbo DELETE -- Exclui um usuario ou retorna "id não cadastrado"
    
    api/associacao     verbo GET    -- Lista todas as associações entre usuários e empresas pelo id
    api/associacao/id  verbo GET    -- Lista uma associação ou retorna "id não cadastrado"
    api/associacao     verbo POST   -- Insere uma associação entre usuário e empresa pelo id
    api/associacao/id  verbo DELETE -- Exclui uma associação ou retorna "id não cadastrado"
</code>
<hr>
<p><b>FRONT</b></p>
<hr>
<p><i>Desenvolvido com</i></p>
<hr>
<p>Angular : v7.0</p>
<hr>
<p><i>Obs.: Front incompleto, devido a problemas com "Cross-Origin Resource Sharing (CORS)", para o localhost não consegui em tempo Hábil implementar as rotas para consumir do Rest, fiz apenas as rotas para empresa, sem conseguir testa-las, mas deixo os fontes para serem avaliados </i></p>
<p><b>DOCKER</b></p>
<hr>
<p>A imagem docker pode ser obitda no link https://hub.docker.com/r/erialex/desafiobry , para baixar <code>docker pull erialex/desafiobry</code>, bastando apenas iniciar os serviços apache e postgresql normalmente atraves do <code>service postgresql start e service apache2 start</code> ficando o rest na pasta "back/server.php/api" </p>
<hr>
## License
<br>
[Licença Láravel](https://opensource.org/licenses/MIT)
<br>
[Licença Apache](https://www.apache.org/licenses/LICENSE-2.0)
<br>
[Licença PostgreSQL](https://www.postgresql.org/about/licence/)
<br>
[Licença PHP](https://en.wikipedia.org/wiki/PHP_License)
<br>
[Licença Ubuntu](https://www.postgresql.org/about/licence/)
<br>
[Licença Angular](https://angular.io/license)
